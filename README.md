# OPR Tools
Userscript for Ingress Operation Portal Recon - https://opr.ingress.com/recon

![](./image/opr-tools.png)

Features:
- Additional links to map services like Intel, OpenStreetMap, bing, Wikimapia and some german ones
- Disabled annoying automatic page scrolling
- Removed "Your analysis has been recorded." dialog
- Moved overall portal rating to same group as other ratings
- Changed portal markers to small circles, inspied by IITC style
- Made "Nearby portals" list and map scrollable with mouse wheel

Download: https://gitlab.com/hedger/opr-tools/raw/master/opr-tools.user.js